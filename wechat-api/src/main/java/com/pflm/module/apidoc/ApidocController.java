package com.pflm.module.apidoc;
import com.alibaba.fastjson.JSONObject;
import com.mongodb.client.FindIterable;
import com.pflm.module.template.service.TemplateService;
import com.pflm.mongodb.MongoSdkBase;
import com.pflm.utils.R;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static com.mongodb.client.model.Filters.eq;

/**
 * 接口文档
 * 访问地址 http://localhost/swagger-ui.html
 * @author qinxuewu
 * @version 1.00
 * @time 14/11/2018下午 2:15
 */

@RestController
@RequestMapping(value="/apidoc")
public class ApidocController {


    @Autowired
    TemplateService templateService;

    @GetMapping("token/getToken/get")
    @ApiOperation("获取微信基础token")
    public R getToken() {
        FindIterable<Document> result = MongoSdkBase.getColl("access_token").find(eq("type",1));
        Document doc=result.first();
        doc.remove("createTime");
        doc.remove("type");
        doc.remove("updateTime");
        return R.ok().put("data",doc);
    }

    @GetMapping("token/getToken/getticket")
    @ApiOperation("获取jsapi_ticket")
    public R getticket() {
        FindIterable<Document> result = MongoSdkBase.getColl("access_token").find(eq("type",2));
        Document doc=result.first();
        doc.remove("createTime");
        doc.remove("type");
        doc.remove("updateTime");
        return R.ok().put("data",doc);
    }


    /**
     * 发送模板消息
     * @param accessToken
     * @return
     */
    @ApiOperation("发送模板模板")
    @PostMapping("template/send")
    public R send(@RequestParam("accessToken") String accessToken, @RequestBody JSONObject info){

        if(StringUtils.isEmpty(accessToken)){
            return R.error("accessToken不能为空");
        }
        if(info==null){
            return R.error("模板消息内容不能为空 不能为空");
        }
        JSONObject data=templateService.delTemplate(accessToken,info);
        return R.ok().put("data",data);
    }
}
